package com.company;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        ArrayList<Microphone> micList = new ArrayList();
        micList.add(new Microphone("Dynamic",6,2,"USB"));
        micList.add(new Microphone("Cors",2,1,"USB"));
        micList.add(new Microphone("Acoustic",3,1,"XLR"));
        Collections.sort(micList, new Microphone.MicrophoneComparatorByType());
        System.out.println(micList);
        Collections.sort(micList, new Microphone.MicrophoneComparatorBySensitivity());
        System.out.println(micList);

        Collections.sort(micList, new Microphone.MicrophoneComparatorByOutput());
        System.out.println(micList);
    }
}
