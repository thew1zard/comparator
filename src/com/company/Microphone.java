package com.company;

import java.util.Comparator;

public class Microphone {
    private String type;
    private int voltage;
    private int soundPressure;

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public int getSoundPressure() {
        return soundPressure;
    }

    public void setSoundPressure(int soundPressure) {
        this.soundPressure = soundPressure;
    }

    private String output;


    public Microphone(String type, int voltage, int soundPressure, String output) {
        this.type = type;
        this.voltage = voltage;
        this.soundPressure = soundPressure;
        this.output = output;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }


    @Override
    public String toString() {
        return "Microphone{" +
                "type='" + type + '\'' +
                "sensitivity= " + voltage/soundPressure +
                ", output='" + output + '\'' +
                '}';
    }

    static class MicrophoneComparatorByType implements Comparator<Microphone> {

        @Override
        public int compare(Microphone o1, Microphone o2) {

            return o1.getType().compareTo(o2.getOutput());
        }


    }

    static class MicrophoneComparatorBySensitivity implements Comparator<Microphone> {


        @Override
        public int compare(Microphone o1, Microphone o2) {
            Integer firstSensitivity = 0;
            Integer sndSensitivity = 0;

            firstSensitivity = o1.getVoltage() / o1.getSoundPressure();
            sndSensitivity = o2.getVoltage() / o2.getSoundPressure();


            return firstSensitivity.compareTo(sndSensitivity);
        }
    }

    static class MicrophoneComparatorByOutput implements Comparator<Microphone> {
        @Override
        public int compare(Microphone o1, Microphone o2) {
            return o1.getOutput().compareTo(o2.getOutput());
        }
    }


}
